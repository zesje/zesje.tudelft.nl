---
title: Welcome to Zesje
category: home
---

*Zesje* is an online software for grading exams.

As its name suggests, Zesje has been designed as a **minimal working prototype** during our free time so do not expect any degree of polish. Still it can provide a lot of advantages over manual grading, especially when dealing with large courses.

## Limitations and disclaimer

+ **Zesje is a prototype-level software**: it is so far tested by people using it. It is not developed by professional web developers, and it certainly has many rough edges.
+ If something breaks, we are very sorry. Most likely we'd be able to fix your problem, but we do not provide any guarantees.
+ Fully trusted users: anyone with the login and password for your Zesje installation can do anything you can.
+ No change history. When we host Zesje there are daily backups, but it does not come out of the box and you cannot revert your changes manually.
+ The current iteration of Zesje assumes that its users are at TU Delft. If you want to use it for outside courses, it will need to be tweaked. We expect to lift this restriction in the future
