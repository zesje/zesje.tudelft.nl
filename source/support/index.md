---
title: Support
---

If you want to use zesje in a course, we are happy to host it for you, just fill out a [form](https://goo.gl/forms/Q6EGv8i5RJWlEaaB3).

We also ask you to sign up for the zesje [support mailing list](https://listserv.tudelft.nl/mailman/listinfo/zesje-tnw) (available from TUD network).

If you have questions about using zesje, please use the following channels:
* if you have a question, use the zesje support chat at https://chat.quantumtinkerer.tudelft.nl/external/channels/zesje (you'll need to create an account and join the **external** team after creating it).
* if you found a bug or if you have a suggestion for improvement, please report it at Zesje [issue tracker](https://gitlab.kwant-project.org/zesje/zesje/issues).