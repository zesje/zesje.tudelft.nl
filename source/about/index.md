---
title: About
---

> From an [article of Delta TU Delft](https://www.delta.tudelft.nl/article/grading-more-systematic-and-less-time-consuming)

In his third-year Solid State Physics bachelor course, lecturer Anton Akhmerov of the faculty Applied Sciences gives weekly mini-tests. Grading those always has to be done under time pressure and costs the course team a lot of work. How could he make grading more systematic and less time-consuming?

Together with programmer Joseph Weston, Akhmerov developed Zesje, a software for online grading.  Zesje is a tool which uses specially formatted exam forms. The exam forms have QR codes so that different pages can be identified automatically. In addition, they have separate boxes for answers and optional templates for multiple choice questions or open questions.

To grade, examiners first scan the exams. Zesje recognises the separate boxes, so when grading a single problem, the examiners only view one box at a time. Graders can choose one or more options from several rubrics with specific errors and negative points, for example: ‘did not compute effective mass: minus 2 points’ or ‘forgot to substitute values, minus four points’. These rubrics also make it easy to give feedback in an automatically generated email to the student.

Zesje is already being used in several courses. According to Akhmerov, Zesje has several benefits.

By grading online, you don’t have to use paper, so the grading is distributed and done in parallel and from anywhere.
- It is faster than paper-based grading, especially for large courses.
- It is easy to check the overall exam statistics and to see how much the score is for each question.
- It enables teachers to email students their exam’s scan together with detailed feedback.
- It is fully open source and is always available for anyone to use.

What about the name? “Zesje is our own estimate of our work,” Akhmerov says. “Its quality is not very high because our developing was done essentially during weekends for a single course. It is a minimally functional prototype. It definitely fulfils its goal to save examiners’ time, but it has glitches. For example if an exam has more than ten pages, the students would receive their scanned exams in a wrong order."

With his fellowship, Akhmerov will hire developers, most likely student-assistants, to make Zesje more robust and reliable. “We are by no means the first ones to come up with the idea of automated grading. Our project is unusual in that we aim to keep Zesje open source and available to anybody who wants to run it. We will not be able to compete with commercial services in terms of quality of support or user-friendliness, but being able to host Zesje on your own or on a university server removes privacy concerns.”


