---
title: Improved PDF processing
date: 2019-05-13 12:50:39
tags:
---
In the past Zesje was very strict on which scanned PDFs it would allow to process. The processing pipeline was built with old Ricoh scanners at the TU Delft in mind. This led to errors with [unsupported PDFs](https://gitlab.kwant-project.org/zesje/zesje/issues/296) and [scans that were not recognized as such](https://gitlab.kwant-project.org/zesje/zesje/issues/295).

We are happy to announce we now support every type of PDF! You are now able to upload every PDF you want, even PDFs that are not scans. Errors like the ones above are now a thing from the past.
