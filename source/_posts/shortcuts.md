---
title: Shortcuts for grading
date: 2019-01-14 13:05:09
tags:
---

We have added a long wanted feature to Zesje: shortcuts! Shortcuts are now available for the grading interface to allow for quicker and easier grading. 

Grading can now be fully controlled with the keyboard, except for adding feedback options. To show available shortcuts hold the `CTRL`-key or click on `Shortcuts` in the top navigation bar. Shortcuts include:

- Navigating between students
- Navigating between problems
- Selecting feedback options

Happy grading!