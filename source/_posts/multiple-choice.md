---
title: Multiple choice questions and grading improvements
date: 2019-10-26 16:20:44
tags:
---

Over the past few months, we have worked on some exciting new features for Zesje. These features include multiple-choice questions, an anonymous grading mode, automatic grading, and shuffling submissions. All are now live and ready to be used. 

<!-- more -->

To start, we have added multiple-choice questions to Zesje. When creating a problem, just mark it as multiple-choice. Zesje will create answer boxes and the corresponding feedback options for you automatically, which you can modify to your liking.

Secondly, to make grading fairer, there is now an anonymous grading mode. When this mode is enabled, the names and student numbers of the students are hidden while grading. Only the copy number of the submission is shown.

Furthermore, Zesje is now able to grade blanks answers and multiple-choice questions for you. By default, Zesje marks the appropriate feedback options and you have to approve them yourself. With auto-approve, there is also the possibility to choose some cases for which you want Zesje to grade and approve answers automatically.

Finally, we have shuffled the submissions for each grader individually. This will limit the amount of collisions when grading the same question with a larger team, while still ensuring a deterministic ordering.