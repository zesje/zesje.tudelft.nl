---
title: Bundling submissions and new overview page
date: 2020-05-04 00:30:05
tags:
---

The Zesje team has implemented bundling of submissions from multiple copies submitted by the same student. That means when grading you will see all student submissions for one problem concatenated.

We have redesigned the overview page and included information on time spent by the graders. We attached some screenshots below or [see the new design in action here](https://sandbox.grading.quantumtinkerer.tudelft.nl/overview/1).

<!-- more -->

We have implemented a "set aside" feature: each time you are unsure whether your grading of a student is final, you may set it aside. The feedback you selected will still stay valid, but zesje will consider the result not final. You can see this feature in action at [the sandbox instance](https://sandbox.grading.quantumtinkerer.tudelft.nl)

Further there are minor accumulated improvements and bugfixes, such as improved image alignment and the ability to rename exams in the interface, ability to export the anonymized exam scans (for example for the visitation committee), extra tooltips, etc.

![](overview-glance.png)
![](overview-histogram.png)
