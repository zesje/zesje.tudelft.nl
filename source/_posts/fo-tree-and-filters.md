---
title: Zesje 0.4.0 released
date: 2021-10-18 17:31:05
tags:
---

We released Zesje 0.4, that implements better security through login management, ability to search for solutions according to the issued feedback, and the introduction of nested feedback options.

In addition to that, the new release contains multiple bugfixes and style updates.

<!-- more -->

## Overview

A picture is worth a thousand words, so here is the grading screen highlighting the new features:

![Zesje grading view showing nested feedback options and filtering by solution.](grade-view.png)

## OAuth 2.0 login management

Previously Zesje users had to share a single login and password, and had to declare who they are in order to grade.

![grader selection dropdown](select_graders.png)

The new version implements an OAuth protocol for login management, where an external application manages the user identities.
Right now the hosted Zesje instances will use the Zesje version control server https://gitlab.kwant-project.org/ for managing login information, however in the future we intend to transition to the TU Delft SAML.

The main changes for the users are:

- You need to use an account on https://gitlab.kwant-project.org/ to log in.
- If you want to add a new grader to your zesje, add the email that they associate with their account on https://gitlab.kwant-project.org/.
- You don't need to select who you are anymore, nor can you impersonate another grader.

## Nested feedback options

Depending on the problem complexity, its grading scheme may become complicated to manage with up to several dozen feedback options. In order to add more structure to the feedback, we allowed feedback options to have an optional nested structure, so that whenever a child option is selected, the parent is automatically selected as well. Conversely, deselecting the parent feedback deselects all of its children.

In order to create a child option of an existing feedback, select the parent option from the dropdown menu next to the "+ option" button.

In the next iteration we will utilize this tree structure to make some feedback options mutually exclusive.

## Searching through submissions

Every now and then we realize that the grading scheme is suboptimal either halfway through the grading process, or even after grading is done. Sometimes we also want to review whether a feedback option was applied consistently.

In the previous Zesje versions searching through submissions with certain feedback required exporting the data as a spreadsheet, and manually opening each submission.

Zesje 0.4 implements filtering submissions by the feedback assigned to them and by grader, so that reviewing and modifying the grading becomes much more efficient.

## Extra

In addition to the shiny new features, we have improved the user experience, and fixed several annoying bugs, so that your grading workflow should become more efficient and streamlined.

Good luck grading!